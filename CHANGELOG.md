# Changelog

## v0.4.3

* Correctly use Feature Policy

## v0.4.2

* Fix on v0.4.1

## v0.4.1

* Don't fail if remote URL returns an empty body

## v0.4.0

* Almost a complete rewrite.
* Does its best to prevent visitor tracking.
* Embed URLs with OEmbed, OGP and fallbacks to discover title and other
  stuff.
* If using Jekyll >= 4.2.0, finds URLs in content and replaces for HTML
  embed.
* Customizable templates.
* Uses [UrlPrivacy](https://0xacab.org/sutty/url-privacy) to prevent
  tracking.

## v0.3.3

* Add `allow-popups` to sandbox so you can open links in a new window.

## v0.3.2

* Rescue `OEmbed::Error`

## v0.3.1

* Put link inside a paragraph so markdown ignores the HTML

## v0.3.0

* Reuse the iframe and sandbox it if the embed code contains one

* Use a Referrer-Policy

  https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Referrer-Policy

  https://web.dev/referrer-best-practices/

## v0.2.0

* Use a sandboxed iframe

## v0.1.0

* Supports OEmbed!
