# frozen_string_literal: true

require_relative 'jekyll/embed'
require_relative 'jekyll/embed/filter'

# Configure Embed
Jekyll::Hooks.register :site, :after_reset do |site|
  Jekyll::Embed.site = site
end
