# frozen_string_literal: true

require 'faraday'
require 'faraday-http-cache'
require 'faraday_middleware/response/follow_redirects'
require 'faraday_middleware/redirect_limit_reached'
require 'loofah'
require 'ogp'
require 'url_privacy'

require_relative 'embed/oembed'
require_relative 'embed/cache'

if Gem::Version.new(Jekyll::VERSION) >= Gem::Version.new('4.2.0')
  require_relative 'embed/content'
else
  Jekyll.logger.warn 'Upgrade to Jekyll >= 4.2.0 to embed URLs in content'
end

OEmbed::Providers.register_all
OEmbed::Providers.register_fallback(OEmbed::ProviderDiscovery,
                                    OEmbed::Providers::Noembed)

module Jekyll
  # The idea with this class is to find the best safe representation of
  # a link.  For a YouTube video it could be the sandboxed iframe.  This
  # loads the video and allows you to reproduce it while preventing YT
  # to call home and send data about your users.  But other social networks
  # will try to take control of their containers by modifying the page.
  # They resist sandboxing and don't work correctly.  For them, we
  # cleanup unwanted HTML tags such as <script>, and return the HTML,
  # which you can style using CSS.  Twitter does this.
  #
  # Others are only available through OGP, so we retrieve the metadata
  # and render a template, which you can provide in your own theme too.
  #
  # We also try for microformats and we would look at Schema.org too but
  # doesn't seem to be a gem for it yet.
  #
  # If the URL doesn't provide anything at all we get the URL, title and
  # date of last visit.
  #
  # Isn't it nice that the corporations that requires us to use OEmbed,
  # OGP, Twitter Cards, Schema.org and other metadata, don't do use
  # themselves?
  #
  # Also we're going to use heavy caching so we don't hit rate limits or
  # lose the representation if the service is down or the URL is
  # removed.  We may be tempted to store the resources locally (images,
  # videos, audio) but we have to take into account that people have
  # legitimate reasons to remove media from the Internet.
  class Embed
    # Attributes to apply by HTMLElement
    IFRAME_ATTRIBUTES = %w[allow sandbox referrerpolicy loading height width].freeze
    IMAGE_ATTRIBUTES = %w[referrerpolicy loading height width].freeze
    MEDIA_ATTRIBUTES = %w[controls height width].freeze
    A_ATTRIBUTES = %w[referrerpolicy rel target].freeze

    # Directive from Feature Policy
    # @see {https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Feature-Policy#directives}
    DIRECTIVES = %w[accelerometer ambient-light-sensor autoplay battery camera display-capture document-domain
                    encrypted-media execution-while-not-rendered execution-while-out-of-viewport fullscreen gamepad geolocation gyroscope layout-animations legacy-image-formats magnetometer microphone midi navigation-override oversized-images payment picture-in-picture publickey-credentials-get speaker-selection sync-xhr usb screen-wake-lock web-share xr-spatial-tracking].freeze

    # Templates
    INCLUDE_OGP = '{% include ogp.html %}'
    INCLUDE_FALLBACK = '{% include fallback.html %}'
    INCLUDE_EMBED = '{% include embed.html %}'

    # The default referrer policy only sends the origin URL (not the
    # full URL, only the protocol/scheme and domain part) if the remote
    # URL is HTTPS.
    #
    # @see {https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Referrer-Policy}
    #
    # The default sandbox restrictions only allow scripts in the context
    # of the iframe and opening new tabs.
    #
    # @see {https://developer.mozilla.org/en-US/docs/Web/HTML/Element/iframe#attr-sandbox}
    DEFAULT_CONFIG = {
      'scrub' => %w[form input textarea button fieldset select option optgroup canvas area map],
      'attributes' => {
        'referrerpolicy' => 'strict-origin-when-cross-origin',
        'sandbox' => %w[allow-scripts allow-popups allow-popups-to-escape-sandbox],
        'allow' => %w[fullscreen; gyroscope; picture-in-picture; clipboard-write;],
        'loading' => 'lazy',
        'controls' => true,
        'rel' => %w[noopener noreferrer],
        'target' => '_blank',
        'height' => nil,
        'width' => nil
      }
    }.freeze

    class << self
      def site
        unless @site
          raise Jekyll::Errors::InvalidConfigurationError,
                'Site is missing, configure with `Jekyll::Embed.site = site`'
        end

        @site
      end

      # This is an initializer of sorts
      #
      # @param [Jekyll::Site]
      # @return [Jekyll::Site]
      def site=(site)
        raise ArgumentError, 'Site must be a Jekyll::Site' unless site.is_a? Jekyll::Site

        @site = site

        # Add the _includes dir so we can provide default templates that
        # can be overriden locally or by the theme.
        includes_dir = File.expand_path(File.join(__dir__, '..', '..', '_includes'))
        site.includes_load_paths << includes_dir unless site.includes_load_paths.include? includes_dir
        # Since we're embedding, we're allowing iframes
        Loofah::HTML5::SafeList::ALLOWED_ELEMENTS_WITH_LIBXML2 << 'iframe'

        reset

        # Other elements that are disallowed
        config['scrub']&.each do |scrub|
          Loofah::HTML5::SafeList::ALLOWED_ELEMENTS_WITH_LIBXML2.delete(scrub)
        end

        payload['embed'] = config['attributes']

        site
      end

      # Reset variables
      #
      # @return [nil]
      def reset
        @allow_same_origin =
          @cache =
            @config =
              @fallback_template =
                @get_cache =
                  @http_client =
                    @info =
                      @ogp_template =
                        @payload =
                          @value_for_attr =
                            nil
      end

      # Render the URL as HTML
      #
      # 1. Try oembed for video and image
      # 2. If rich oembed, cleanup
      # 3. If OGP, render templates
      # 4. Else, render fallback template
      #
      # @param [String] URL
      # @return [String] HTML
      def embed(url)
        raise URI::Error unless url.is_a? String

        url = url.strip

        # Quick check
        raise URI::Error unless url.start_with? 'http'

        # Just to verify the URL is valid
        # TODO: Use Addressable
        URI.parse url

        oembed(url) || ogp(url) || fallback(url) || url
      rescue URI::Error
        Jekyll.logger.warn "#{url.inspect} is not a valid URL"

        url
      end

      # @return [Hash]
      def config
        @config ||= Jekyll::Utils.deep_merge_hashes(DEFAULT_CONFIG, (site.config['embed'] || {})).tap do |c|
          c['attributes']['allow'].concat (DIRECTIVES - c.dig('attributes', 'allow').join.split(';').map do |s|
                                                          s.split(' ').first
                                                        end).join(" 'none';|").split('|')
        end
      end

      # Try for OEmbed
      #
      # @param [String] URL
      # @return [String,NilClass] Sanitized HTML or nil
      def oembed(url)
        cache.getset("oembed+#{url}") do
          oembed = OEmbed::Providers.get url

          # Prevent caching of nil?
          raise OEmbed::Error unless oembed.respond_to? :html

          context = info.dup
          context[:registers][:page] = payload['page'] = cleanup(oembed.html, url)

          embed_template.render!(payload, context)
        end
      rescue OEmbed::Error
        nil
      end

      # Try for OGP.
      # @param [String] URL
      # @return [String,NilClass]
      def ogp(url)
        cache.getset("ogp+#{url}") do
          ogp = OGP::OpenGraph.new get(url).body
          page = {
            locale: ogp.locales.first,
            title: ogp.title,
            url: ogp.url,
            description: ogp.description,
            type: ogp.type,
            data: ogp.data
          }.transform_keys(&:to_s)

          %w[image video audio].each do |attr|
            page[attr] = ogp.public_send(:"#{attr}s").find do |a|
              a && a.url && http?(a.url)
            end&.url
          end

          context = info.dup
          context[:registers][:page] = payload['page'] = page

          cleanup ogp_template.render!(payload, context), url
        end
      rescue ArgumentError
        Jekyll.logger.warn 'Invalid contents (OGP):', url
        nil
      rescue LL::ParserError, OGP::MalformedSourceError, OGP::MissingAttributeError, Faraday::Error
        nil
      end

      # Try something
      def fallback(url)
        cache.getset("fallback+#{url}") do
          html        = Nokogiri::HTML.fragment get(url).body
          element     = html.css('article').first
          element   ||= html.css('section').first
          element   ||= html.css('main').first
          element   ||= html.css('body').first
          title       = html.css('title').first
          description = html.css('meta[name="description"]').first

          context = info.dup
          context[:registers][:page] = payload['page'] = {
            'title' => text(title),
            'description' => text(description),
            'url' => url,
            'image' => element&.css('img')&.first&.public_send(:[], 'src'),
            'locale' => html.css('html')&.first&.public_send(:[], 'lang')
          }

          cleanup fallback_template.render!(payload, context), url
        end
      rescue ArgumentError
        Jekyll.logger.warn 'Invalid contents (fallback):', url
        nil
      rescue Faraday::Error, Nokogiri::SyntaxError
        nil
      end

      # @param [String] URL
      # @return [Faraday::Response]
      def get(url)
        @get_cache ||= {}
        @get_cache[url] ||= http_client.get url
      end

      # @return [Jekyll::Embed::Cache]
      def cache
        @cache ||= Jekyll::Embed::Cache.new('Jekyll::Embed')
      end

      # @return [Faraday::Connection]
      def http_client
        @http_client ||= Faraday.new do |builder|
          builder.options.timeout = 4
          builder.options.open_timeout = 1
          builder.options.read_timeout = 1
          builder.options.write_timeout = 1
          builder.use FaradayMiddleware::FollowRedirects
          builder.use :http_cache, shared_cache: false, store: cache, serializer: Marshal
        end
      end

      # @params :html_fragment [String]
      # @params :url [String]
      # @return [String]
      def cleanup(html_fragment, url)
        html = Loofah.fragment(html_fragment).scrub!(:prune)

        # Add our own attributes
        html.css('iframe').each do |iframe|
          IFRAME_ATTRIBUTES.each do |attr|
            set_value_for_attr(iframe, attr)
          end

          # Embedding itself require allow-same-origin
          iframe['sandbox'] += allow_same_origin(url)
        end

        html.css('audio, video').each do |media|
          MEDIA_ATTRIBUTES.each do |attr|
            set_value_for_attr(media, attr)
          end

          media['src'] = UrlPrivacy.clean media['src']
        end

        html.css('img').each do |img|
          IMAGE_ATTRIBUTES.each do |attr|
            set_value_for_attr(img, attr)
          end
        end

        html.css('a').each do |a|
          A_ATTRIBUTES.each do |attr|
            set_value_for_attr(a, attr)
          end
        end

        html.css('[src]').each do |element|
          element['src'] = UrlPrivacy.clean(element['src'])
        end

        html.css('[href]').each do |element|
          element['href'] = UrlPrivacy.clean(element['href'])
        end

        # Return the cleaned up HTML as a String
        html.to_s
      end

      def text(node)
        node&.text&.tr("\n", '')&.tr("\r", '')&.strip&.squeeze(' ')
      end

      private

      def embed_template
        @embed_template ||= site.liquid_renderer.file('embed.html').parse(INCLUDE_EMBED)
      end

      def fallback_template
        @fallback_template ||= site.liquid_renderer.file('fallback.html').parse(INCLUDE_FALLBACK)
      end

      def ogp_template
        @ogp_template ||= site.liquid_renderer.file('ogp.html').parse(INCLUDE_OGP)
      end

      def info
        @info ||= {
          registers: { site: site },
          strict_filters: site.config.dig('liquid', 'strict_filters'),
          strict_variables: site.config.dig('liquid', 'strict_variables')
        }
      end

      # @param [String]
      # @return [String]
      def value_for_attr(attr)
        @value_for_attr ||= {}
        @value_for_attr[attr] ||=
          case (value = config.dig('attributes', attr))
          when Array then value.join(' ')
          else value
          end
      end

      # @param element [Nokogiri::XML::Element]
      # @param attr [String]
      # @return [nil]
      def set_value_for_attr(element, attr)
        case (value = value_for_attr(attr))
        when NilClass, FalseClass then element.delete(attr)
        when TrueClass then element[attr] = nil
        else element[attr] = value
        end
      end

      # If the iframe comes from the same site, we can allow the same
      # origin policy on the sandbox.
      #
      # @param [String] URL
      # @return [String]
      def allow_same_origin(url)
        unless site.config['url']
          Jekyll.logger.warn 'Add url to _config.yml to determine if the site can embed itself'
          return ' allow-same-origin'
        end

        @allow_same_origin ||= {}
        @allow_same_origin[url] ||= url.start_with?(site.config['url']) ? '' : ' allow-same-origin'
      end

      # Caches it because Jekyll::Site#site_payload returns a new object
      # everytime.
      #
      # @return [Jekyll::Drops::UnifiedPayloadDrop]
      def payload
        @payload ||= site.site_payload
      end

      # Discover if a string is a valid HTTP URL
      #
      # @param :url [String]
      # @return [Boolean]
      def http?(url)
        url.start_with?('http') && URI.parse(url).is_a?(URI::HTTP)
      rescue URI::Error
        false
      end
    end
  end
end
