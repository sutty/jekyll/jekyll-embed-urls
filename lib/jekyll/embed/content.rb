# frozen_string_literal: true

require 'nokogiri'

module Jekyll
  class Embed
    class Content
      class << self
        # Find URLs on paragraphs.  We do it after rendering because
        # sometimes we use HTML instead of pure Markdown and this way we
        # catch both.
        #
        # @param :content [String]
        # @return [String]
        def embed(content)
          Nokogiri::HTML5.fragment(content).tap do |html|
            html.css('p > a').each do |a|
              next unless a.parent.children.size == 1
              next unless a['href'] == a.text.strip

              embed = Jekyll::Embed.embed a['href']

              next if a['href'] == embed

              a.parent.replace embed
            end
          end.to_s
        end
      end
    end
  end
end

Jekyll::Hooks.register :posts, :post_convert do |post|
  post.content = Jekyll::Embed::Content.embed post.content
end
