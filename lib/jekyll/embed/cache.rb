# frozen_string_literal: true

module Jekyll
  class Embed
    # Jekyll cache that behaves like ActiveSupport::Cache
    class Cache < Jekyll::Cache
      def write(key, value)
        self[key] = value
      end

      def read(key)
        self[key]
      rescue StandardError
        nil
      end
    end
  end
end
