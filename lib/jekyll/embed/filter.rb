# frozen_string_literal: true

module Jekyll
  class Embed
    module Filter
      # This filter takes the URL passed as input an returns its HTML
      # representation.  Embed takes care of everything else.
      def embed(url)
        return url unless url.is_a? String

        Embed.embed url
      end

      def oembed(url)
        return url unless url.is_a? String

        Embed.oembed url
      end

      def ogp(url)
        return url unless url.is_a? String

        Embed.ogp url
      end

      def fallback(url)
        return url unless url.is_a? String

        Embed.fallback url
      end
    end
  end
end

Liquid::Template.register_filter(Jekyll::Embed::Filter)
