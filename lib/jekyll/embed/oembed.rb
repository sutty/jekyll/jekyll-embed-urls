# frozen_string_literal: true

require 'oembed'

module OEmbed
  module ProviderDecorator
    def self.included(base)
      base.class_eval do
        private def http_get(url, _)
          Jekyll::Embed.get(url.to_s).body
        rescue Faraday::Error
          raise OEmbed::UnknownResponse
        end
      end
    end
  end
end

OEmbed::Provider.include OEmbed::ProviderDecorator
OEmbed::ProviderDiscovery.singleton_class.include OEmbed::ProviderDecorator
