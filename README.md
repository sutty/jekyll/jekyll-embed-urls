# jekyll-embed-urls

This plugin converts URLs to their previsualization by using
[OEmbed](https://oembed.com/), [OGP](http://ogp.me/).  It fallbacks to
showing a card with basic information.

While developing this plugin, we found out that OEmbed providers tend to
inject JavaScript and other ways of tracking users, so this plugin does
its best to prevent it.

For OGP and fallback, you can modify the templates.

## Installation

Add this line to your site's Gemfile:

```ruby
gem 'jekyll-embed-urls'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install jekyll-embed-urls

## Usage

Add the plugin to your `_config.yml`:

```yaml
plugins:
- jekyll-embed-urls
embed:
# Extra elements to remove
  scrub:
  - form
  - input
  - textarea
  - button
  - fieldset
  - select
  - option
  - optgroup
  - canvas
  - area
  - map
# Attribute values can be strings or array of strings, or nil/false for
# removing them, or true to add them without value
  attributes:
    referrerpolicy: strict-origin-when-cross-origin
    sandbox:
    - allow-scripts
    - allow-popups
    allow:
    - fullscreen;
    - gyroscope;
    - picture-in-picture;
    - clipboard-write;
    loading: 'lazy'
    controls: true
    width: nil
    height: nil
    rel:
    - noopener
    - noreferrer
    target: _blank
```

Then, when you want to embed an URL (like a video) in a post, simply
drop it in a single line with empty lines around, as such:

```
---
layout: post
title: A video
---

Hi, I really liked this video:

https://www.invidio.us/watch?v=XYHnd4boUoM
```

**Note:** The URL can be anywhere, at the start, end or between
paragraphs but it needs to be in its own block of text.

**Another note:** [Invidious doesn't support OEmbed
yet](https://github.com/omarroth/invidious/issues/1222) :P

## Themes

You can also use it as a Liquid filter, for instance:

```html
{{ page.embed_url | embed }}
```

The `embed` filter takes an URL and replaces it for the HTML.  Other
filters are `oembed`, `ogp` and `fallback`.

### Templates

You can modify the templates by providing your own include files,
`_includes/ogp.html` and `_includes/fallback.html`.  We don't add any
CSS so you can develop your own.

To access default includes, run `bundle show jekyll-embed-urls` and copy
the files from the `_includes` directory to your site.

## Facebook and Instagram

Facebook deprecated their OEmbed API and now a token is required for
embedding Facebook and Instagram URLs.  Set it as an environtment
variable named `OEMBED_FACEBOOK_TOKEN`.

If you don't have it, this plugin make a best effort attempt.  Instagram
will be available through OGP, but their image URLs expire after
a certain time, so your site may appear broken after a while.  We could
download them but we decided not to because it may infringe on
intellectual property laws and personal rights such as privacy, and
consequently put our service at risk.

It's our position that there're legitimate uses for downloading remote
media, such as for archiving collective memory (police brutality, public
figures speeches, etc.) that may be removed without notice.

In these cases our recommendation is always not to host with corporate
services, since they don't share our politics and actively work against
us.

We're hotlinking and copying text though, assuming that falls under fair
use rights.

## Tracking prevention

Anti-tracking techniques implemented are:

* `<script>` and other tags are removed.  No external JS is loaded in
  a local context.

* `<form>`s and their elements are removed.

* `<canvas>`, `<area>`, `<map>` are removed.

* `<iframe>`s are
  [sandboxed](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/iframe#attr-sandbox).

* `<img>`s are lazy loaded.  This is not strickly anti-tracking but
  images are loaded when needed.

* All URLs get their tracking params removed by
  [UrlPrivacy](https://0xacab.org/sutty/url-privacy)

* [Referrer
  Policy](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Referrer-Policy)
  is implemented for supported elements.  Extrangely, `<video>` and
  `<audio>` don't seem to support it.

* External links open in a new tab and have `rel="noopener noreferrer"`
  to prevent [reverse
  tabnabbing](https://owasp.org/www-community/attacks/Reverse_Tabnabbing).

If you find more useful techniques, please [open an issue
report](https://0xacab.org/sutty/jekyll/jekyll-embed-urls/-/issues).

## Feature policy

[Feature
policy](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Feature-Policy)
is a list of directives for allowing or denying features.

The directives are separated by semicolons.  Any directive not mentioned
in the configuration is assumed to have a "none" policy by this plugin.

## Contributing

Bug reports and pull requests are welcome on 0xacab.org at
<https://0xacab.org/sutty/jekyll/jekyll-embed-urls>. This project is
intended to be a safe, welcoming space for collaboration, and
contributors are expected to adhere to the [Sutty code of
conduct](https://sutty.nl/en/code-of-conduct/).

If you like our plugins, [please consider
donating](https://donaciones.sutty.nl/en/)!

## License

The gem is available as free software under the terms of the GPL3
License.

## Code of Conduct

Everyone interacting in the jekyll-linked-posts project’s codebases,
issue trackers, chat rooms and mailing lists is expected to follow the
[code of conduct](https://sutty.nl/en/code-of-conduct/).
