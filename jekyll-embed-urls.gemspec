# frozen_string_literal: true

Gem::Specification.new do |spec|
  spec.name          = 'jekyll-embed-urls'
  spec.version       = '0.6.1'
  spec.authors       = %w[f]
  spec.email         = %w[f@sutty.nl]

  spec.summary       = 'Embed URL previsualization in Jekyll posts'
  spec.description   = 'Replaces URLs for their previsualization in Jekyll posts'
  spec.homepage      = "https://0xacab.org/sutty/jekyll/#{spec.name}"
  spec.license       = 'GPL-3.0'
  spec.required_ruby_version = Gem::Requirement.new('>= 2.7')

  spec.metadata = {
    'bug_tracker_uri' => "#{spec.homepage}/issues",
    'homepage_uri' => spec.homepage,
    'source_code_uri' => spec.homepage,
    'changelog_uri' => "#{spec.homepage}/-/blob/master/CHANGELOG.md",
    'documentation_uri' => "https://rubydoc.info/gems/#{spec.name}"
  }

  spec.files         = Dir['lib/**/*', '_includes/*']
  spec.require_paths = %w[lib]

  spec.extra_rdoc_files = Dir['README.md', 'CHANGELOG.md', 'LICENSE.txt']
  spec.rdoc_options += [
    '--title', "#{spec.name} - #{spec.summary}",
    '--main', 'README.md',
    '--line-numbers',
    '--inline-source',
    '--quiet'
  ]

  spec.add_dependency 'jekyll', '~> 4'
  spec.add_dependency 'ruby-oembed', '~> 0.16.0'
  spec.add_dependency 'loofah', '~> 2.9'
  spec.add_dependency 'ogp', '~> 0.5.0', '< 0.6'
# Someday :)
# spec.add_dependency 'microformats', '~> 4.2'
  spec.add_dependency 'faraday', '~> 1.3'
  spec.add_dependency 'faraday-http-cache', '~> 2.2'
  spec.add_dependency 'faraday_middleware', '~> 1'
  spec.add_dependency 'url-privacy', '~> 0.1.1'
  spec.add_development_dependency 'rubocop'
end
